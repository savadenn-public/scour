# Scour

Docker image to run [scour](https://github.com/scour-project/scour)

Image repository:

- [registry.gitlab.com/savadenn-public/scour:latest](https://gitlab.com/savadenn-public/scour/container_registry)
- [savadenn/scour:latest](https://hub.docker.com/r/savadenn/scour)

## Usage

```bash
docker run --rm savadenn/scour <same option as scour>
```
